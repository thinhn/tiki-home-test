<?php
	require_once('passwordManager.php');
	require_once('userManager.php');

	class Validate
	{
		protected static $instance = null;

		public static function getInstance()
		{
			if (self::$instance == null) {
				self::$instance =  new self();
			}
			return self::$instance;
		}

		public function __construct() 
		{
	    }

	    public function password($password)
	    {
	    	echo json_encode(PasswordManager::getInstance()->validatePassword($password));
	    }

	    public function username($username)
	    {
	    	echo json_encode(UserManager::getInstance()->validateUsername($username));
	    }
	}