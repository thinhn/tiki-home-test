<?php
	/**
	 * 
	 */
	class PasswordManager
	{
		protected static $instance = null;
		public $username;
		public $encrypted_password;

		public static function getInstance()
		{
			if (self::$instance == null) {
				self::$instance =  new self();
			}
			return self::$instance;
		}

		public function __construct() 
		{
	    }

	    protected function encrypt($password)
	    {
	    	return md5($password);
	    }

	    protected function verifyPassword($password)
	    {
	    	$encrypted_password = $this->encrypt($password);
	    	if ($encrypted_password == $this->encrypted_password) return true;
	    	return  false;
	    }

		public function validatePassword($password) 
		{	    	
			// check whitespace
			if (preg_match('/\s/',$password)) {
				$message['whitespace'] = 0;
			} else {
				$message['whitespace'] = 1;
			}
			// check length string 6 > 
			if (strlen($password) <= 6) {
				$message['length'] = 0;
			} else {
				$message['length'] = 1;
			}
			// check uppercase and lowercase
			$regex = "/^(?=.*[a-z])(?=.*[A-Z]).*$/";
			if (!preg_match($regex, $password)) {
				$message['upper_lower'] = 0;
			} else {
				$message['upper_lower'] = 1;
			}

			// check digit and symbol
			$regex = "/^(?=.*\d)(?=.*[!@#$%^&*]).*$/";
			if (!preg_match($regex, $password)) {
				$message['digit_symbol'] = 0;
			} else {
				$message['digit_symbol'] = 1;
			}

			return $message;
		}

		public function setNewPassword($password) 
		{
			$validate = $this->validatePassword($password);
			foreach ($validate as $key => $value) {
				if ($value != 1) return false;
			}
			return $this->encrypt($password);
		}
	}