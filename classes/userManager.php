<?php
	require_once('fileManager.php');
	/**
	 * 
	 */
	class UserManager
	{
		protected static $instance = null;

		public static function getInstance()
		{
			if (self::$instance == null) {
				self::$instance =  new self();
			}
			return self::$instance;
		}

		public function __construct() 
		{
	    }

	    public function getUser($username)
	    {
	    	return FileManager::getInstance()->getInfoByUsername($username);
	    }

		public function validateUsername($username) 
		{
			// check whitespace
			if (preg_match('/\s/',$username)) {
				$message['username_whitespace'] = 0;
			} else {
				$message['username_whitespace'] = 1;
			}
			// check length string 6 > 
			if (strlen($username) <= 6) {
				$message['username_length'] = 0;
			} else {
				$message['username_length'] = 1;
			}
			
			// check username a->z A->Z 0->9 _
			if ( !preg_match('/^[A-Za-z0-9_]+$/', $username)) {
				$message['username_letter_number'] = 0;
	        } else {
	        	$message['username_letter_number'] = 1;
	        }


	        $message['username_exist'] = 0;	

	        if ($message['username_letter_number'] == 1 && $message['username_length'] == 1 && $message['username_whitespace'] == 1) {
		        if ($this->getUser($username)) {
		        	$message['username_exist'] = 1;	
		        }
	        }

			return $message;
		}

		public function checkExistUser($username = null) 
		{
			$file = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.'password.txt';
			$handle = fopen($file, "r");
			if ($handle) {
			    while (($line = fgets($handle)) !== false) {
			    	$data = explode('|', $line);
			    	if ($data[0] == $username) {
			    		return $data;
			    	}
			    }
			    fclose($handle);
			}
		    return false;
		}

		public function saveUser($username, $encrypted_password)
		{
			$fileManager = FileManager::getInstance();
			return $fileManager->saveFile($username, $encrypted_password);
		}

		public function updateUser($username, $encrypted_password)
		{
			$fileManager = FileManager::getInstance();
			return $fileManager->updateFile($username, $encrypted_password);
		}		
	}