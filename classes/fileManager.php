<?php
	/**
	 * 
	 */
	class FileManager
	{
		protected static $instance = null;

		public static function getInstance()
		{
			if (self::$instance == null) {
				self::$instance =  new self();
			}
			return self::$instance;
		}

		public function __construct() 
		{
	    }

	    public function getInfoByUsername($username)
	    {
	    	$path = $_SERVER['DOCUMENT_ROOT'];
			$filename = $path.DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.'password.txt';
			if (file_exists($filename)) {
		    	$handle = fopen($filename, "r");
		        if ($handle) {
		            while (($line = fgets($handle)) !== false) {
		                $data = explode('|', $line);
		                if ($data[0] == $username) {
		                    return $data;
		                }
		            }
		            fclose($handle);
		        }
			}
	        return false;
	    }

	    public function saveFile($username, $encrypted_password)
	    {
	    	$path = $_SERVER['DOCUMENT_ROOT'];
			$filename = $path.DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.'password.txt';
			$handle = fopen($filename, "a");
			$str = $username.'|'.$encrypted_password.'|'.PHP_EOL;
			fwrite($handle, $str);
			fclose($handle);
			return true;
	    }

	    public function updateFile($username, $encrypted_password)
	    {
	    	$flag = false;
	    	$path = $_SERVER['DOCUMENT_ROOT'];
			$filename = $path.DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.'password.txt';
	        $content = file($filename);
	        foreach ($content as $line_num => $line) {
	            $data = explode('|', $line);
	            if ($data[0] == $username) {
	            	$flag = true;
	                $str = $username.'|'.$encrypted_password.'|'.PHP_EOL;
	                $content[$line_num] = $str;
	            }
	        }
	        file_put_contents($filename, $content);
	        return $flag;
	    }

	}