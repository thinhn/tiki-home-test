<?php
	require_once('../classes/passwordManager.php');
	require_once('../classes/userManager.php');
	

	class User extends PasswordManager
	{
		protected static $instance = null;

		public static function getInstance()
		{
			if (self::$instance == null) {
				self::$instance =  new self();
			}
			return self::$instance;
		}

		public function __construct() 
		{
			session_start();
	    }

		public function login($username, $password) 
		{
			$userManager = UserManager::getInstance();

			$data = $userManager->getUser($username);
			if (!$data) return false;
			$this->encrypted_password = $data[1];
			if ($this->verifyPassword($password)) {
				$_SESSION['username'] = $username;
				return true;
			}
			return false;
		}

		public function register($username, $password)
		{
			$userManager = UserManager::getInstance();

			if ($userManager->getUser($username)) return false;
			$encrypted_password = $this->encrypt($password);
			if (!$encrypted_password) return false;
			$userManager->saveUser($username, $encrypted_password);
			return true;
		}

		public function changePassword($username, $password, $new_password)
		{
			$userManager = UserManager::getInstance();

			$data = $userManager->getUser($username);
			if (!$data) return false;
			$this->encrypted_password = $data[1];
			if ($this->verifyPassword($password)) {
				$encrypted_password = $this->setNewPassword($new_password);
				if ($encrypted_password) {
					return $userManager->updateUser($username, $encrypted_password);
				}
				return false;
			}
		}
	}