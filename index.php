<?php 
	session_start(); 
	if (empty($_SESSION['username']) || !isset($_SESSION['username'])) {
		header("Location: /login");
		exit();
	}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Tiki</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <?php require_once("./elements/head.php"); ?>
    </head>
    <body>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="wrapper-page">
                            <div class="m-t-40 card-box">
                				<a href="/login" class="btn btn-xs btn-warning pull-right" type="submit">Log out</a>
                				<form id="form_change" class="form-horizontal" method="POST">
	                                <div class="account-content">
	                                    <div class="text-center m-b-20">
	                                        <h3 class="expired-title"><?= $_SESSION['username'] ?></h3>
	                                    </div>
	                                    <form id="form_change" class="form-horizontal" method="POST">
                                            <input id="username" name="username" type="hidden" value="<?= $_SESSION['username' ]?>">
	                                        <div class="form-group m-b-20">
	                                            <div class="col-xs-12">
	                                                <label for="password">Password</label>
	                                                <input id="password" name="password" class="form-control" type="password" placeholder="Enter your old password">
	                                            </div>
	                                        </div>
	                                        <div class="form-group m-b-20">
	                                            <div class="col-xs-12">
	                                                <label for="password">New Password</label>
	                                                <input id="new_password" name="new_password" class="form-control" type="password" placeholder="Enter your new password">
	                                            </div>
	                                        </div>
	                                        <ul>
	                                            <li id="whitespace" class="text-danger">The password must not contain any whitespace</li>
	                                            <li id="length" class="text-danger">The password must be at least 6 characters long</li>
	                                            <li id="upper_lower" class="text-danger">The password must contain at least one uppercase and at least one lowercase letter</li>
	                                            <li id="digit_symbol" class="text-danger">The password must have at least one digit and symbol</li>
	                                        </ul>
	                                        <div class="form-group account-btn text-center m-t-10">
	                                            <div class="col-xs-12">
	                                                <button id="btn_change" class="btn btn-lg btn-primary btn-block">Change password</button>
	                                            </div>
	                                        </div>
	                                    </form>
	                                    <div class="clearfix"></div>
	                                </div>
                            	</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php require_once("./elements/script.php"); ?>
    </body>
</html>