$('#username').bind('keyup', function(e) {
    var username = $("#username").val();
    var data = {"username": username};
    validate(data);
});

$('#password').bind('keyup', function(e) {
    var password = $("#password").val();
    var data = {"password": password};
    validate(data);
});

$('#new_password').bind('keyup', function(e) {
    var password = $("#new_password").val();
    var data = {"new_password": password};
    validate(data);
});

$(document).on('click', '#btn_change', function() {
    var username = $('#username').val();
    var password = $('#password').val();
    var new_password = $('#new_password').val();
    var data = {"username": username, "password": password, "new_password": new_password};
    $("#form_change").submit(function(e) {
        $.ajax({
            url: '/ajax/changePassword.php',
            type: 'POST',
            dataType: 'html',
            data: data
        }).done(function(res) {
            var res = JSON.parse(res);
            if (res.status) {
                swal(
                  'Good job!',
                  'Password changed!',
                  'success'
                )
                setTimeout(function(){ location.reload(); }, 1500);
            } else {
                swal(
                  'Error!',
                  'Please re-enter the information.',
                  'error'
                )
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
});

$(document).on('click', '#btn_register', function() {
    var username = $('#username').val();
    var password = $('#password').val();
    var data = {"username": username, "password": password};
    $("#form_register").submit(function(e) {
        $.ajax({
            url: '/ajax/register.php',
            type: 'POST',
            dataType: 'html',
            data: data
        }).done(function(res) {
            var res = JSON.parse(res);
            if (res.status) {
                swal(
                  'Good job!',
                  'User registered successfully!',
                  'success'
                )
                setTimeout(function(){ location.reload(); }, 1500);
            } else {
                swal(
                  'Error!',
                  'Please re-enter the information.',
                  'error'
                )
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
});


$(document).on('click', '#btn_login', function() {
    var username = $('#username').val();
    var password = $('#password').val();
    var data = {"username": username, "password": password};
    $("#form_login").submit(function(e) {
        $.ajax({
            url: '/ajax/login.php',
            type: 'POST',
            dataType: 'html',
            data: data
        }).done(function(res) {
            console.log(res);
            var res = JSON.parse(res);
            if (res.status) {
                window.location.href = "/";
            } else {
                swal(
                  'Error!',
                  'Username or password incorrect.',
                  'error'
                )
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
});

function validate(data) 
{
    $.ajax({
        url: '/ajax/validate.php',
        type: 'POST',
        dataType: 'html',
        data: data
    }).done(function(res) {
        var res = JSON.parse(res);
        for (var key in res) {
            if (key == "username_exist") {
                if (res[key] == 1) {
                    $('#username_exist').show();
                } else {
                    $('#username_exist').hide();
                }
                continue;
            }
            var text_class = "text-danger";
            if (res[key] == 1) {
                var text_class = "text-success";
            }
            $('#'+key).removeClass();
            $('#'+key).addClass(text_class);
        }
    });
}

