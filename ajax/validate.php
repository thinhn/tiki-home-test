<?php
	require_once('../classes/validate.php');

	if (isset($_POST['password'])) {
		$password = $_POST['password'];
		$validate = Validate::getInstance()->password($password);
	}

	if (isset($_POST['username'])) {
		$username = $_POST['username'];
		$validate = Validate::getInstance()->username($username);
	}

	if (isset($_POST['new_password'])) {
		$new_password = $_POST['new_password'];
		$validate = Validate::getInstance()->password($new_password);
	}

?>

