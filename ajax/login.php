<?php
require_once('../controllers/user.php');
if (!empty($_POST['username']) && !empty($_POST['password'])) {
	$user = User::getInstance();
	$result['status'] = $user->login($_POST['username'], $_POST['password']);
} else {
	$result['status'] = false;
}
echo json_encode($result);