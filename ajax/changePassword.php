<?php
require_once('../controllers/user.php');
if (!empty($_POST['username']) && !empty($_POST['password']) && !empty($_POST['new_password'])) {
	$user = User::getInstance();
	$result['status'] = $user->changePassword($_POST['username'], $_POST['password'], $_POST['new_password']);
} else {
	$result['status'] = false;
}
echo json_encode($result);