<?php 
    session_start();
    $_SESSION['username'] = '';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Tiki</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <?php require_once("./elements/head.php"); ?>
        <style type="text/css">
            #username_exist {
                display: none;
            }
        </style>
    </head>
    <body>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="wrapper-page">
                            <div class="m-t-40 card-box">
                                <div class="text-center">
                                    <h2 class="text-uppercase m-t-0 m-b-30">
                                        <a href="/" class="text-success">
                                            Register
                                        </a>
                                    </h2>
                                </div>
                                <div class="account-content">
                                    <ul id="message_error">
                                        <li id="username_exist" class="text-danger">The username exist!</li>
                                    </ul>
                                    <form id="form_register" class="form-horizontal" method="POST">
                                        <div class="form-group m-b-20">
                                            <div class="col-xs-12">
                                                <label for="username">Username</label>
                                                <input name="username" id="username" class="form-control" type="text" required="" placeholder="Enter your username">
                                            </div>
                                        </div>
                                        <ul>
                                            <li id="username_whitespace" class="text-danger">The username must not contain any whitespace</li>
                                            <li id="username_length" class="text-danger">The username must be at least 6 characters long</li>
                                            <li id="username_letter_number" class="text-danger">The username only can a->z, A->Z, 0->9, _</li>
                                        </ul>
                                        <div class="form-group m-b-20">
                                            <div class="col-xs-12">
                                                <label for="password">Password</label>
                                                <input name="password" id="password" class="form-control" type="password" required="" placeholder="Enter your password">
                                            </div>
                                        </div>
                                        <ul>
                                            <li id="whitespace" class="text-danger">The password must not contain any whitespace</li>
                                            <li id="length" class="text-danger">The password must be at least 6 characters long</li>
                                            <li id="upper_lower" class="text-danger">The password must contain at least one uppercase and at least one lowercase letter</li>
                                            <li id="digit_symbol" class="text-danger">The password must have at least one digit and symbol</li>
                                        </ul>
                                        <div class="form-group account-btn text-center m-t-10">
                                            <div class="col-xs-12">
                                                <button id="btn_register" name="register" class="btn btn-lg btn-primary btn-block" >Sign Up </button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="row m-t-50">
                                <div class="col-sm-12 text-center">
                                    <p class="text-muted">Already have an account?  <a href="/login" class="text-dark m-l-5">Sign In</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php require_once("./elements/script.php"); ?>
    </body>
</html>